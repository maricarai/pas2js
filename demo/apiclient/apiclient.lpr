
program apiclient;
{:< - A Unit **@name** demonstra como o pas2js, usa o valor dos elementos da página
      html em execução e gera automaticamente o código de serviço.

  - **DESCRIÇÃO**
    - Usando a classe **tapiclientCodeGen** da unidade **FPRPCCodeGen** (disponível em FPC
      nativo e em pas2js), a descrição JSON pode ser consumida e uma unidade.

    - O código de serviço pode ser gerado automaticamente. A unidade gerada conterá uma
      classe de serviço para cada classe exposta pelo servidor FPC JSON-RPC.

    - A distribuição PAS2JS contém um projeto de demonstração (apicliente) que usa esta
      unidade e permite você para gerar as classes de serviço expostas por um servidor,
      100% automaticamente.

    - A versão do tronco do código do servidor FPC **JSON-RPC** pode gerar
      este código automaticamente, você pode obtê-lo digitando o seguinte URL no navegador:

      ```pascal

        http://localhost:3000/RPC/API?format=pascal&unitname=services

      ```

    - Dessa forma, a descrição do seu serviço pode ser regenerada a qualquer momento para que sempre reflita exatamente o que o servidor está esperando como entrada e quais dados está retornando.
    - Como o servidor **JSON RPC** suporta apenas os tipos **JSON**, o código gerado pode usar apenas o tipos de JSON genéricos ao gerar código. Uma extensão está planejada onde as dicas do tipo podem ser dado e, por exemplo, um tipo de registro pode ser especificado em vez de um **TJsonObject** genérico classe, ou um tempo **tdateTime** em vez de uma **string**.

    - **REFERÊNCIAS**
      - [Pas2JS: Communicating with the webserver](https://www.freepascal.org/~michael/articles/pas2js2/pas2js2.pdf)

  - **NOTAS**
    - Este cliente depende do servidor rpc ?????
    - Arquivos originais do projeto:
      - apiclient.lpi
      - apiclient.lpr
      - bulma.min.css
      - index.html

    - A página html associada a este código contém os seguintes inputs:

       ```html
         <input id="edtURL" class="input" type="text" placeholder="URL where to reach FPC API">
         <input id="edtUnit" class="input" type="text" placeholder="Unit name">
         <input id="cbPreferNativeInt"  type="checkbox" checked> Prefer NativeInt
         <input id="cbForceJSValueResult" type="checkbox"> Force JSValue result in callbacks
         <button id="btnGenerate" class="button is-link">Generate unit</button>
       ```

  - **CÓDIGO FONTE**:
    - @html(<a href="../apiclient.lpr">apiclient.lpr</a>)

  - **HISTÓRICO**
    - Documento criado por: paulosspacheco@@yahoo.com.br
      - **14/03/2023** : Inicio da documentação deste código
}

{$mode objfpc}

uses
  browserapp, JS, Classes, SysUtils, Web, fpjson, fpjsonjs, fprpccodegen, unit1;

type

  { TMyApplication }
  {: - A Class **@name** permite interagir com os elementos da página html ativo no
        browser.

  }
  TMyApplication = class(TBrowserApplication)
    {< - O atributo **@name** é usado para receber a reposta da requisição ao servidor rpc}
    edtResult : TJSHTMLTextAreaElement;

    edtURL : TJSHTMLInputElement;
    edtUnit : TJSHTMLInputElement;
    cbPreferNativeInt : TJSHTMLInputElement;
    cbForceJSValueResult : TJSHTMLInputElement;
    btnGenerate : TJSHTMLButtonElement;
    procedure BindElements;
    procedure doRun; override;
  private
    function DoGenerateCode(aEvent: TJSMouseEvent): boolean;
    procedure GenerateAPI(const aJSON: String);
  end;

procedure TMyApplication.BindElements;
begin
  edtResult:=TJSHTMLTextAreaElement(GetHTMLElement('edtResult'));
  edtURL:=TJSHTMLInputElement(GetHTMLElement('edtURL'));
  edtUnit:=TJSHTMLInputElement(GetHTMLElement('edtUnit'));
  cbPreferNativeInt:=TJSHTMLInputElement(GetHTMLElement('cbPreferNativeInt'));
  cbForceJSValueResult:=TJSHTMLInputElement(GetHTMLElement('cbForceJSValueResult'));
  btnGenerate:=TJSHTMLButtonElement(GetHTMLElement('btnGenerate'));
  btnGenerate.OnClick:=@DoGenerateCode;
end;

procedure TMyApplication.doRun;

begin
  BindElements;
  Terminate;
end;

Procedure TMyApplication.GenerateAPI(const aJSON: String);

Var
  API : TJSONObject;
  Gen : TAPIClientCodeGen;
  Opts : TClientCodeOptions;

begin
  API:=GetJSON(aJSON) as TJSONObject;
  Opts:=[];
  if cbForceJSValueResult.checked then
    Include(Opts,ccoForceJSValueResult);
  if cbPreferNativeInt.Checked then
    Include(Opts,ccoPreferNativeInt);
  Gen:=TAPIClientCodeGen.Create(Self);
  try
    Gen.API:=API;
    Gen.Options:=Opts;
    Gen.OutputUnitName:=edtUnit.Value;
    Gen.Execute;
    edtResult.value:=Gen.Source.Text;
  finally
    Gen.Free;
  end;
end;

function TMyApplication.DoGenerateCode(aEvent: TJSMouseEvent): boolean;

  procedure GenAPI(Resp : TJSResponse); async;

  begin
    GenerateAPI(Await(Resp.text()));
  end;

  function DoOK(aValue: JSValue): JSValue;

  var
    Resp : TJSResponse absolute aValue;

  begin
    Result:=undefined;
    GenAPI(Resp)
  end;

  function DoFail(aValue: JSValue): JSValue;
  begin
    Result:=undefined;
    window.alert('Failed to fetch API description at URL '+edtURL.value)
  end;

begin
  Result:=True;
  window.fetch(edtURL.Value,TJSObject.New)._then(@DoOK,@DoFail);
end;

var
  Application : TMyApplication;

begin
  Application:=TMyApplication.Create(nil);
  Application.Initialize;
  Application.Run;
end.
